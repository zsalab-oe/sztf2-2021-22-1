﻿using System;

namespace _3_Tipus_Atalakitas
{
    class Allat { }
    class Kutya : Allat { }
    class Macska : Allat { }

    class Program
    {
        static void Main(string[] args)
        {
            double pi = 3.14;
            int x = (int)pi;  // x = 3

            Allat cirmos = new Macska();
            Allat amoba = new Allat();
            Macska kormos;
            Kutya bodri;
            kormos = (Macska)cirmos;


            //bodri = (Kutya)kormos;
            // fordításidejű hiba (Macska típusú referenciával nem is lehetne Kutya típusú objektumra hivatkozni)

            //kormos = (Macska)amoba;
            // futásidejű hiba (Állat típusú referenciával akár lehetne is Macska típusú objektumra hivatkozni - azonban az amoba referencia éppen egy Állat típusú objektumra hivatkozik, így nem sikerül majd az átalakítás, de ez csak futásidőben derül ki)



            kormos = cirmos as Macska;
            if (amoba is Kutya)
                bodri = amoba as Kutya;

            // A kasztolás kivételt dob, az 'as' operátor null-t ad vissza, ha nem sikerült az átalakítás


            Console.ReadLine();
        }
    }
}
