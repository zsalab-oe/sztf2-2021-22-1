﻿using System;

namespace _2_Polimorfizmus
{
    class Allat
    {
        public void Bemutatkozas()
        {
            Console.WriteLine("Szia, állat vagyok.");
        }

        public virtual void Leiras()
        {
            Console.WriteLine("Általában állatkodom.");
        }
    }

    class Emlos : Allat
    {
        public new void Bemutatkozas()
        {
            Console.WriteLine("Szia, emlős vagyok.");
        }

        public override void Leiras()
        {
            //base.Leiras();
            Console.WriteLine("Szülök.");
        }
    }

    class Kutya : Emlos
    {
        public new void Bemutatkozas()
        {
            Console.WriteLine("Szia, kutya vagyok.");
        }

        public override void Leiras()
        {
            Console.WriteLine("Ugatok.");
        }
    }

    class Madar : Allat
    {
        public new void Bemutatkozas()
        {
            Console.WriteLine("Szia, madár vagyok.");
        }

        public override void Leiras()
        {
            Console.WriteLine("Repülök.");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Kutya morzsa = new Kutya();
            Emlos fules = new Kutya();

            /* nem-virtuális metódusok használata */

            morzsa.Bemutatkozas();
            fules.Bemutatkozas();

            /* virtuális metódusok használata */

            morzsa.Leiras();
            fules.Leiras();


            Allat[] allatok = new Allat[]
                {
                    new Allat(),
                    new Emlos(),
                    new Kutya(),
                    new Madar(),
                    morzsa
                };
            //Allat[] teszt = new Allat[5];
            //teszt[4] = morzsa;


            // *****************************************
            // Polimorfizmus nem-virtuális metódussal? Így nincs polimorfizmus!
            // lefuttatva látható, hogy mindenkinél az Allat osztályban létrehozott metódus fut le

            Console.WriteLine("===============");
            for (int i = 0; i < allatok.Length; i++)
            {
                allatok[i].Bemutatkozas();
            }

            // Polimorfizmus virtuális metódussal
            // lefuttatva látható, hogy mindenkinél a hivatkozott OBJEKTUM típusának megfelelő osztály metódusa fut le

            Console.WriteLine("===============");
            for (int i = 0; i < allatok.Length; i++)
            {
                allatok[i].Leiras();
            }


            Console.ReadLine();
        }
    }
}