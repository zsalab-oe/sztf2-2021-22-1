﻿using System;

namespace _1_Orokles_Alapok
{
    class Allat
    {
        //prop + Tab + Tab
        public int Eletkor { get; set; }
        public void Fut()
        {

        }

        private int labszam;
        protected string kedvencEledel;

        protected int Labszam { get { return labszam; } }

        public Allat(int labszam)
        {
            this.labszam = labszam;
        }

        public Allat(string kedvencEledel)
        {
            this.kedvencEledel = kedvencEledel;
        }

    }

    class Emlos : Allat
    {
        public bool KicsinyetEteti()
        {
            return true;
        }

        //ctor + Tab + Tab

        // kötelező meghívni az ősosztály (Allat) valamely konstruktorát még a leszármazott (Emlos) konstruktorának lefutása előtt --> "base" kulcsszóval
        public Emlos(int labszam) : base("tej")
        {

        }
    }

    class Kutya : Emlos
    {
        public void FarkatCsovalja()
        {
            if (this.kedvencEledel != null && this.Labszam == 4)
                ; // TODO
        }
        public void Ugat() { }

        private int farokHossz;

        public Kutya(int labszam, int farokHossz)
            : base(labszam)
        {
            this.farokHossz = farokHossz;
        }
    }

    class Pok : Allat
    {
        public Pok(int labszam)
            : base(labszam)
        {

        }

        public Pok()
            : base(8)
        {

        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            // Típuskompatibilitás
            // Minden T típusú objektumra hivatkozhatunk T típusú vagy T bármelyik őse típusú referenciával
            // egyenlőségjel bal oldalán: referencia
            // egyenlőségjel jobb oldalán: objektum
            // az ős referenciáján keresztül a leszármazottakban felvett új mezők és metódusok nem érhetők el


            Allat elsoAllat = new Allat(5);
            //elsoAllat.
            Emlos masodikAllat = new Emlos(4);
            Allat harmadikAllat = new Emlos(2);
            //masodikAllat.
            //harmadikAllat.
            Kutya negyedikAllat = new Kutya(4, 15);
            //negyedikAllat.
            //negyedikAllat.
            Pok otodikAllat = new Pok();

        }
    }
}
