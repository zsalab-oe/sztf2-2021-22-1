﻿using System;

namespace _4_Abstract_Sealed_Class
{
    abstract class Ember { }  // absztrakt osztály
    abstract class Jatekos : Ember { } // absztrakt osztály
    class Kapus : Jatekos { }
    sealed class Tamado : Jatekos { } // lezárt osztály

    //class Csatar : Tamado { } // Hibás, lezárt osztályból nem engedi a származtatást

    class Program
    {
        static void Main(string[] args)
        {
            //Ember e = new Ember(); // Hibás, absztrakt osztályból nem lehet példányosítani
            //Jatekos j = new Jatekos(); // szintén

            Tamado t = new Tamado(); // OK
            Jatekos valaki = new Tamado(); // OK

            // Lényegében:
            // abstract class >> ne lehessen példányt létrehozni belőle
            // sealed class >> ne lehessen származtatni belőle
        }
    }
}
