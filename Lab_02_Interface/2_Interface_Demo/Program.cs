﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Interface_Demo
{
    // Explicit interfész megvalósítás
    // - Ha több megvalósított interfész is tartalmaz ugyanolyan metódus szignatúrát (+ maga a megvalósító osztály is tartalmazhat ilyen metódust), akkor azok különböző metódussal is megvalósíthatók
    // - Ebben az esetben a hívást végző referencia típusától függ, hogy melyik metódus fut le

    interface IEgyik
    {
        void M();
    }

    interface IMasik
    {
        void M();
    }

    class Osztaly : IEgyik, IMasik
    {
        void IEgyik.M()
        {
            // egyikféle implementáció
        }

        void IMasik.M()
        {
            // másik megvalósítás
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            IEgyik o1 = new Osztaly();
            o1.M();

            IMasik o2 = new Osztaly();
            o2.M();

            Osztaly o3 = new Osztaly();
            //o3.M(); // hibás, az interfészek által előírt metódusok csak interfész referenciákon keresztül elérhetőek (ezért is nem lehet őket ellátni láthatósági jelzővel)
        }
    }
}
