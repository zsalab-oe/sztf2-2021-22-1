﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Futar
{
    class Webshop
    {
        IFutarceg futarceg;

        // a Webshop osztálynak nem is kell tudnia, hogy valójában melyik futárcég szállítja majd a rendeléseket (milyen típusú lesz a futárcég mögött rejlő objektum)
        // => gyakorlatilag úgy cserélhetjük le a futárcéget (egy jobbra), hogy a Webshop osztályt nem kell hozzá módosítanunk !!!
        public Webshop(IFutarceg futarceg)
        {
            this.futarceg = futarceg;
        }

        public void Rendeles()
        {
            Program.LogToConsole("[Webshop] - Rendelését rögzítettük.");
            string TN = futarceg.Szallit();
            Program.LogToConsole("[Webshop] - Csomagkövetési azonosító: " + TN);
        }
    }
}
