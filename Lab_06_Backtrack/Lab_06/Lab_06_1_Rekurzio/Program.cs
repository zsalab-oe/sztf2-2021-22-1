﻿using System;

namespace Lab_06_1_Rekurzio
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] tomb = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            int[] tomb2 = new int[] { 3, 4, 6, 8, 12, 14, 20, 21, 25 };

            Console.WriteLine(Sum(tomb));

            Console.WriteLine(LogKer(tomb2, 6));

            Console.ReadLine();
        }

        static int Sum(int[] tomb)
        {
            return SumRek(tomb, tomb.Length - 1);
        }

        static int SumRek(int[] tomb, int n)
        {
            if (n == -1)
                return 0;

            else
                return SumRek(tomb, n - 1) + tomb[n];
        }

        static int LogKer(int[] tomb, int keresettErtek)
        {
            return LogKerRek(tomb, 0, tomb.Length - 1, keresettErtek);
        }

        static int LogKerRek(int[] tomb, int bal, int jobb, int keresettErtek)
        {
            if (bal > jobb)
                return -1;

            int center = (bal + jobb) / 2;
            if (keresettErtek < tomb[center])
                return LogKerRek(tomb, bal, center - 1, keresettErtek);
            if (keresettErtek > tomb[center])
                return LogKerRek(tomb, center + 1, jobb, keresettErtek);
            else
                return center;
        }
    }
}
