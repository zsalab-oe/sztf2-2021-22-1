﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_Kivetel_Bank
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Bankszamla jozsiSzamla = new Bankszamla("Józsi", 5000);

                Felvetel(jozsiSzamla, 1000);
                Felvetel(jozsiSzamla, 6000);
                jozsiSzamla.Aktiv = false;
                Felvetel(jozsiSzamla, 200);
                Felvetel(jozsiSzamla, 90000);
            }
            catch (Exception e)
            {
                Console.WriteLine("Még mindig ismeretlen hiba. :( " + e.Message);
            }

            Console.ReadLine();
        }

        static void Felvetel(Bankszamla szamla, int osszeg)
        {
            Console.WriteLine($"Felvétel történik... ({osszeg} Ft)");
            try
            {
                szamla.PenzFelvetel(osszeg);
                Console.WriteLine("A pénzfelvétel sikerült.");
            }
            catch (PenzFelvetelException e)
            {
                Console.WriteLine($"{DateTime.Now} - {e.Szamla.Tulajdonos} számláján nincs elegendő összeg, hiányzik: {e.HianyzoOsszeg}.");
            }
            catch (Exception e)
            {
                Console.WriteLine("Ismeretlen hiba. " + e.Message);
                throw; // kivétel továbbdobása
            }
        }

    }
}
