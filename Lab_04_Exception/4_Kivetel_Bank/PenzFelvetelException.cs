﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_Kivetel_Bank
{
    class PenzFelvetelException : BankszamlaException
    {
        public decimal HianyzoOsszeg { get; }

        public PenzFelvetelException(decimal HianyzoOsszeg, Bankszamla Szamla)
            : base(Szamla) 
        {
            this.HianyzoOsszeg = HianyzoOsszeg;
        }

        public PenzFelvetelException(decimal HianyzoOsszeg, Bankszamla Szamla, string msg)
            : base(Szamla, msg)
        {
            this.HianyzoOsszeg = HianyzoOsszeg;
        }
    }
}
