﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_Kivetel_Bank
{
    class Bankszamla
    {
        public string Tulajdonos { get; private set; }
        decimal egyenleg;
        public bool Aktiv { get; set; }

        public Bankszamla(string tulajdonos, int egyenleg)
        {
            Tulajdonos = tulajdonos;
            this.egyenleg = egyenleg;
            Aktiv = true;
        }

        public void PenzBefizetes(decimal osszeg)
        {
            if (!Aktiv)
            {
                throw new BankszamlaException(this);
            }

            egyenleg += osszeg;
        }

        public void PenzFelvetel(decimal osszeg)
        {
            // bonyolult előkészületek...
            // ezek a plusz (amúgy felesleges) metódushívások csak azt szemléltetik, hogy nem kell mindig közvetlenül a problémás kódrészeket tartalmazó metódushívást körbevenni try-catch blokkal
            // a lényeg, hogy a hívási láncban valahol kezeljük, kapjuk el az eldobott kivételeket
            PenzFelvetelSegedMetodus1(osszeg);
        }


        private void PenzFelvetelSegedMetodus1(decimal osszeg)
        {
            // további bonyolult előkészületek lehetnének itt is...
            PenzFelvetelSegedMetodus2(osszeg);
        }

        private void PenzFelvetelSegedMetodus2(decimal osszeg)
        {
            if (!Aktiv)
            {
                throw new BankszamlaException(this);
            }
            if (osszeg > this.egyenleg)
            {
                decimal hianyzik = osszeg - this.egyenleg;
                throw new PenzFelvetelException(hianyzik, this);
            }

            egyenleg -= osszeg;
        }
    }
}
