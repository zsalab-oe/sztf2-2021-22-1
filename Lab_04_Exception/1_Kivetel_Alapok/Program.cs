﻿using System;

namespace _1_Kivetel_Alapok
{
    class Program
    {
        static void Main(string[] args)
        {
            int x, y, eredmeny;

            x = 10;
            y = 0;

            try // a problémás kódrészeket try blokkba tesszük
            {
                eredmeny = x / y;
                Console.WriteLine(eredmeny);
            }
            catch (DivideByZeroException e) // itt kezeljük le a kivételt
            {
                //Console.WriteLine("Hiba: " + e.Message);
                Console.WriteLine("Valamimt elszúrtál");
            }
            finally
            {
                Console.WriteLine("A finally blokk minden esetben lefut.");
            }


            // try
            //   - védett (próbára tett) szakasz
            //   - ide tesszük a problémás kódrészeket (ahol kivétel keletkezhet)
            // catch
            //   - hibakezelő szakasz
            //   - szűkíthetjük, hogy milyen típusú kivételt szeretnénk kezelni a kivétel típus megadásával
            //   - egy változóval a catch blokkon belül hivatkozhatunk az eldobott kivételobjektumra, ami a hibakezelés szempontjából további fontos információkkal szolgálhat (pl.: Message)
            // finally
            //   - ez a blokk mindig lefut (attól függetlenül, hogy keletkezett-e kivétel vagy sem)


            Console.ReadLine();
        }
    }
}
