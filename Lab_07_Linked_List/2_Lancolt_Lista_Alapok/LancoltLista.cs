﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Lancolt_Lista_Alapok
{
    class NincsIlyenElemException : Exception
    {
        public NincsIlyenElemException(string message) : base(message)
        { }
    }

    delegate void Muvelet(string tartalom);

    class LancoltLista
    {
        // beágyazott osztály (osztály osztályon belül)
        // A "ListaElem" osztály és annak mezői csak a "LancoltLista" osztályon belülről érhetőek el, így mezőik az egyszerűség érdekében lehetnek publikusak
        class ListaElem
        {
            public string tartalom;
            public ListaElem kovetkezo;
        }

        private ListaElem fej;

        public LancoltLista()
        {
            fej = null;
        }

        public void BeszurasElejere(string tartalom)
        {
            ListaElem uj = new ListaElem();
            uj.tartalom = tartalom;
            uj.kovetkezo = fej;
            fej = uj;
        }

        public void BeszurasVegere(string tartalom)
        {
            ListaElem uj = new ListaElem();
            uj.tartalom = tartalom;
            uj.kovetkezo = null;

            if (fej == null)
            {
                fej = uj;
            }
            else
            {
                ListaElem p = fej;
                while (p.kovetkezo != null)
                {
                    p = p.kovetkezo;
                }
                p.kovetkezo = uj;
            }
        }

        public void Torles(string torlendo)
        {
            ListaElem p = fej;
            ListaElem e = null;
            while (p != null && !p.tartalom.Equals(torlendo))
            {
                e = p;
                p = p.kovetkezo;
            }
            if (p != null)
            {
                if (e == null)                      // megvan (első elem)
                    fej = p.kovetkezo;
                else                                // megvan (többi elem között)
                    e.kovetkezo = p.kovetkezo;
            }
            else                                    // nem talált VAGY üres lista
            {
                throw new NincsIlyenElemException($"Nincs ilyen elem ({torlendo}) a listában.");
            }
        }


        public void Bejaras(Muvelet metodus)
        {
            Muvelet _metodus = metodus;

            ListaElem p = fej;
            while (p != null)
            {
                _metodus?.Invoke(p.tartalom);       // tartalom "feldolgozása"
                p = p.kovetkezo;                    // léptetés a következő listaelemre
            }
        }

        // Megj:
        // _metodus?.Invoke(p.tartalom);
        // ugyanaz, mint ez (csak egyszerűsítve):
        // if (_metodus != null)
        //     _metodus(p.tartalom);
    }
}
