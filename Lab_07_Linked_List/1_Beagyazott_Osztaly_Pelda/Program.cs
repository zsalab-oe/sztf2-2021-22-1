﻿using System;

namespace _1_Beagyazott_Osztaly_Pelda
{
    // C#-ban lehetőségünk van egy osztályt egy másik osztályon belül deklarálni
    // akkor célszerű használni, ha egy osztály működéséhez szükségünk van egy segédosztályra / segédobjektumra, amire más osztályoknak egyáltalán nincs szükségük, sőt a (segéd)osztályt el is szeretnénk rejteni más osztályok elől
    // ilyen esetben indokolt a beágyazott osztályok használata
    // A "Belso" nevű osztály a "Kulso" számáralátható
    // (Egyéb osztályok nem látják a "Belso"osztályt)

    class Kulso
    {

        // beágyazott osztály (osztály osztályon belül)
        class Belso
        {
            // mivel a "Belso" osztály csak a "Kulso" számára látható, (és eleve a "Kulso" osztály számára hoztuk létre a segédosztályt), így megengedett a publikus mezők használata
            public int x;
        }


        // A "Kulso" osztály metódusai hozzáférnek a "Belso" osztály mezőihez
        void Metodus()
        {
            Belso segedValtozo = new Belso();
            segedValtozo.x = 5;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            // hibás
            // innen eleve a típust se látjuk,így értelemszerűen a mezőihezse tudunk hozzáférni
            Belso seged = new Belso();
            seged.x = 5;
        }
    }
}
