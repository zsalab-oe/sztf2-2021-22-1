﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_Generikus_Lancolt_Lista
{
    delegate void Muvelet<T_del>(T_del tartalom);

    class LancoltLista<T_lista>
    {
        class ListaElem
        {
            public T_lista tartalom;
            public ListaElem kovetkezo;
        }

        ListaElem fej;

        public LancoltLista()
        {
            fej = null;
        }

        public void BeszurasElejere(T_lista tartalom)
        {
            ListaElem uj = new ListaElem();
            uj.tartalom = tartalom;
            uj.kovetkezo = fej;
            fej = uj;
        }

        public void Bejaras(Muvelet<T_lista> metodus)
        {
            Muvelet<T_lista> _metodus = metodus;

            ListaElem p = fej;
            while (p != null)
            {
                _metodus?.Invoke(p.tartalom);
                p = p.kovetkezo;
            }
        }
    }
}
