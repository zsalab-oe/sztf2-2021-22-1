﻿using System;

namespace _3_Generikus_Peldak
{
    // Generikus osztályok

    class EgyszeruTarolo<T> // <T> - típusparaméter
    {
        public T Adat { get; set; }
    }

    class Tarolo<T> where T : class // megszorítás ('T' csak referenciatípus lehet)
    { /* ... */ }

    class LeszarmazottTarolo<T> : Tarolo<T> where T : class // megszorítások nem öröklődnek, azokat meg kell ismételni
    { /* ... */ }


    static class Eszkozok
    {
        // Generikus metódusok

        public static T Kisebb<T>(T egyik, T masik) where T : IComparable
        {
            if (egyik.CompareTo(masik) < 0)
                return egyik;
            else
                return masik;
        }

        public static void Csere<T>(ref T egyik, ref T masik)
        {
            T seged = egyik;
            egyik = masik;
            masik = seged;
        }
    }


    class Szemely { /* ... */ }

    class Program
    {
        static void Main(string[] args)
        {
            EgyszeruTarolo<int> tarolo1 = new EgyszeruTarolo<int>();
            tarolo1.Adat = 20;

            EgyszeruTarolo<Szemely> tarolo2 = new EgyszeruTarolo<Szemely>();
            tarolo2.Adat = new Szemely();

            //Tarolo<int> tarolo3 = new Tarolo<int>(); // hibás, 'T' csak referenciatípus lehet
            Tarolo<Szemely> tarolo4 = new Tarolo<Szemely>(); // OK

            int x = 5;
            int y = 100;
            Console.WriteLine(x + ", " + y);
            Eszkozok.Csere<int>(ref x, ref y);
            Console.WriteLine(x + ", " + y);

            // metódus hívásánál a típusparaméter megadása elhagyható, a fordító megállapítja a paraméterként átadott változók típusából
            Eszkozok.Csere(ref x, ref y);
            Console.WriteLine(x + ", " + y);


            Console.ReadLine();
        }
    }
}
