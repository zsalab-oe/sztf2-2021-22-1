﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5_Generikus_Lancolt_Lista_Bejaroval
{
    class Program
    {
        static void Main(string[] args)
        {
            LancoltLista<string> lista = new LancoltLista<string>();
            lista.BeszurasElejere("Dénes");
            lista.BeszurasElejere("Cecil");
            lista.BeszurasElejere("Béla");
            lista.BeszurasElejere("Aladár");
            

            Console.WriteLine(">> foreach");

            foreach (string nev in lista)
            {
                // a "nev" ciklusváltozó a Current tulajdonság értékét veszi fel
                Console.WriteLine(nev);
            }

            
            Console.WriteLine("\n>> while");                // mi is kérhetünk egy bejárót a listától

            IEnumerator<string> bejaro = lista.GetEnumerator();

            while (bejaro.MoveNext())                       // nekünk kell léptetni
            {
                Console.WriteLine(bejaro.Current);          // majd lekérhetjük a lista aktuális elemét
            }

            
            Console.WriteLine("\n>> while + feltétel");     // megadhatjuk, meddig szeretnénk bejárni a listát

            bejaro.Reset();

            while (bejaro.MoveNext() && bejaro.Current != "Cecil")
            {
                Console.WriteLine(bejaro.Current);
            }


            Console.ReadLine();
        }
    }
}
