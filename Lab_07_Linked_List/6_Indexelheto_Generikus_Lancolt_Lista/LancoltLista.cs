﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6_Indexelheto_Generikus_Lancolt_Lista
{
    class LancoltLista<T>
    {
        class ListaElem
        {
            public T tartalom;
            public ListaElem kovetkezo;
        }

        private ListaElem fej;

        private int elemszam;
        public int Elemszam { get { return elemszam; } }

        public LancoltLista()
        {
            fej = null;
        }

        public void BeszurasElejere(T tartalom)
        {
            ListaElem uj = new ListaElem();
            uj.tartalom = tartalom;
            uj.kovetkezo = fej;
            fej = uj;

            this.elemszam++;
        }

        ListaElem Kereses(int index)
        {
            ListaElem p = fej;
            int j = 0;
            while (p != null && j < index)
            {
                p = p.kovetkezo;
                j++;
            }
            if (p != null)
            {
                return p;
            }
            else
            {
                throw new ArgumentOutOfRangeException(nameof(index));
            }
        }


        // Bővebben: https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/indexers/
        public T this[int index]
        {
            get
            {
                ListaElem p = Kereses(index);
                return p.tartalom;
            }
            set
            {
                ListaElem p = Kereses(index);
                p.tartalom = value;
            }
        }
    }
}
