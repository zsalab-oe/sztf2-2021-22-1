﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6_Indexelheto_Generikus_Lancolt_Lista
{
    class Program
    {
        static void Main(string[] args)
        {
            LancoltLista<string> lancoltLista = new LancoltLista<string>();
            lancoltLista.BeszurasElejere("Dénes");
            lancoltLista.BeszurasElejere("Cecil");
            lancoltLista.BeszurasElejere("Béla");
            lancoltLista.BeszurasElejere("Aladár");

            string valaki = lancoltLista[3];
            Console.WriteLine(valaki);

            lancoltLista[3] = "XXX";
            Console.WriteLine(lancoltLista[3]);

            Console.ReadLine();
        }
    }
}
