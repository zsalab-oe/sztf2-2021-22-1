﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Hash_Table
{
    abstract class HasitoTablazat<K, T>
    {
        protected int m; // méret

        protected HasitoTablazat(int m)
        {
            this.m = m;
        }

        // hasítófüggvény: a megadott kulcsból előállít egy érvényes indexet
        protected virtual int h(K kulcs)
        {
            return Math.Abs(kulcs.GetHashCode()) % m;
        }

        public abstract void Beszuras(K kulcs, T ertek);
        public abstract T Kereses(K kulcs);
        public abstract void Torles(K kulcs);

        public T this[K kulcs]
        {
            get { return Kereses(kulcs); }
            set { Beszuras(kulcs, value); }
        }
    }
}
