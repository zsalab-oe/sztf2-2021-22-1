﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Hash_Table
{
    class Torta
    {
        public Gyumolcs Gyumolcs { get; }
        public int Atmero { get; set; }

        public Torta(Gyumolcs gyumolcs, int atmero)
        {
            Gyumolcs = gyumolcs;
            Atmero = atmero;
        }

        public override string ToString()
        {
            return $"{Gyumolcs.ToString()} torta ({Atmero} cm)";
        }
    }
}
