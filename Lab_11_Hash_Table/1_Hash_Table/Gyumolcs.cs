﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Hash_Table
{
    class Gyumolcs
    {
        public string Nev { get; }
        public Gyumolcs(string nev)
        {
            Nev = nev;
        }

        public override int GetHashCode()
        {
            return Nev.GetHashCode();
        }

        public override string ToString()
        {
            return Nev;
        }
    }
}
