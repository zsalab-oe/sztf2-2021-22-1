﻿using System;
using System.Collections.Generic;

namespace _1_Hash_Table
{
    class Program
    {
        static void Main(string[] args)
        {
            Gyumolcs b = new Gyumolcs("banán");
            Gyumolcs c = new Gyumolcs("citrom");
            Gyumolcs e = new Gyumolcs("eper");
            Torta t1 = new Torta(b, 10);
            Torta t2 = new Torta(c, 20);
            Torta t3 = new Torta(e, 30);

            // kulcs: Gyümölcs, tartalom: Torta

            HasitoTablazat<Gyumolcs, Torta> ht = new HasitoTablazatLancoltListaval<Gyumolcs, Torta>(13);
            ht.Beszuras(t1.Gyumolcs, t1);
            ht.Beszuras(t2.Gyumolcs, t2);
            ht[t3.Gyumolcs] = t3;

            Console.WriteLine(ht[e]);

            Console.WriteLine("-------------------");

            // Ugyanaz a példa a beépített Dictionary osztállyal:

            Dictionary<Gyumolcs, Torta> dict = new Dictionary<Gyumolcs, Torta>(13);
            dict.Add(t1.Gyumolcs, t1);
            dict.Add(t2.Gyumolcs, t2);
            dict[t3.Gyumolcs] = t3;

            Console.WriteLine(dict[e]);

            Console.ReadLine();
        }
    }
}
