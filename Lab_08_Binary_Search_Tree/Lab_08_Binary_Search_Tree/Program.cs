﻿using System;

namespace Lab_08_Binary_Search_Tree
{
    class Program
    {
        static void Main(string[] args)
        {
            BinarisKeresoFa<string, int> fa = new BinarisKeresoFa<string, int>();
            fa.Beszuras("Ferenc", 6);
            fa.Beszuras("Cecil", 3);
            fa.Beszuras("Elemér", 5);
            fa.Beszuras("Ilona", 9);
            fa.Beszuras("Bela", 2);
            fa.Beszuras("Aladár", 1);
            fa.Beszuras("Gábor", 7);
            fa.Beszuras("Dénes", 4);
            fa.Beszuras("Huba", 8);

            fa.Bejaras(Console.WriteLine);

            Console.ReadLine();
        }
    }
}
