﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_08_Binary_Search_Tree
{
    delegate void Muvelet<T>(T tartalom);

    class BinarisKeresoFa<T, K> where K : IComparable
    {
        class FaElem
        {
            public T tartalom;
            public K kulcs;
            public FaElem bal, jobb;

            public FaElem()
            {

            }

            public FaElem(T tartalom, K kulcs)
            {
                this.tartalom = tartalom;
                this.kulcs = kulcs;
                bal = null;
                jobb = null;
            }
        }

        FaElem gyoker;

        public BinarisKeresoFa()
        {
            gyoker = null;
        }

        // a beszúrást elindító publikus metódus
        public void Beszuras(T tartalom, K kulcs)
        {
            Beszuras(ref gyoker, tartalom, kulcs);
        }

        // rekurzív beszúrást megvalósító, privát metódus
        // beszúrás közben meg kell keresnünk a beszúrandó elem megfelelő helyét, hogy megtartsuk a rendezettséget
        void Beszuras(ref FaElem p, T tartalom, K kulcs)        // p-t referencia szerint adjuk át!
        {
            if (p == null)                                      // megvan a beszúrandó elem leendő helye
            {
                p = new FaElem(tartalom, kulcs);                // beszúrás
            }
            else
            {
                if (p.kulcs.CompareTo(kulcs) > 0)               // ha a beszúrandó elem kulcsa kisebb, mint az aktuális faelemé
                {
                    Beszuras(ref p.bal, tartalom, kulcs);       // akkor balra megyünk tovább*
                }
                else                                            // ha a beszúrandó elem kulcsa nagyobb...
                {
                    Beszuras(ref p.jobb, tartalom, kulcs);      // akkor jobbra megyünk tovább*
                }
            }
        }
        // *mivel minden faelemre igaznak kell lennie, hogy tőle balra csak kisebb, tőle jobbra csak nagyobb kulcsú faelemek helyezkedhetnek el


        Muvelet<T> _metodus;

        public void Bejaras(Muvelet<T> metodus)
        {
            _metodus = metodus;
            InOrder(gyoker);
            _metodus = null;

            // Megj.: a delegálton keresztül történő metódushívást meg lehetne oldani máshogy is, most azért döntöttem e megoldás mellett, hogy a bejárást megvalósító InOrder() metódus paraméterlistáját ne kelljen módosítani, jobban hasonlítson az előadásjegyzetben ismertetett algoritmusra
        }

        void InOrder(FaElem p)
        {
            if (p != null)
            {
                InOrder(p.bal);
                _metodus?.Invoke(p.tartalom);
                InOrder(p.jobb);
            }
        }
    }
}
