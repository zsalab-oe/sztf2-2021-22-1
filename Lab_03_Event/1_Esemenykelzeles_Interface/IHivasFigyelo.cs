﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Esemenykelzeles_Interface
{
    interface IHivasFigyelo
    {
        void BejovoHivasTortent(Telefon kuldo, string forras_telefonszam);
        void KimenoHivasTortent(Telefon kuldo, string cel_telefonszam);
    }
}
