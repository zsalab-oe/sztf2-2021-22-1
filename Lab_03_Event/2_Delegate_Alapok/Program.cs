﻿using System;

namespace _2_Delegate_Alapok
{
    // delegált definiálása
    public delegate void SzovegKozvetito(string szoveg);

    public delegate double MuveletKozvetito(double szam);

    delegate void KulsoMuvelet(int a, int b);


    class Program
    {
        static void Main(string[] args)
        {
            // Metódushívás képviselőn (delegálton) keresztül:
            //   - példányt ebből is létre lehet hozni
            //   - ehhez a konstruktorának át kell adni a későbbiekben hívandó metódust
            //   - figyeljünk, hogy az átadott metódus (és a később feliratkoztatott metódusok) mind egyforma szignatúrával kell rendelkezzenek!
            //   - a metódus szignatúráját a delegált típusa írja elő (amit fent adtunk meg)

            SzovegKozvetito szovegFeldolgozo;

            // feliratkoztatás, "gyűjtőbe belerakás"
            szovegFeldolgozo = new SzovegKozvetito(KiirKicsi);
            szovegFeldolgozo += new SzovegKozvetito(KiirNagy);
            // a feliratkoztatott metódus(ok) meghívása delegálton keresztül
            // minden metódust meghív az adott paraméterrel
            szovegFeldolgozo("Teszt Üzenet");


            szovegFeldolgozo -= KiirNagy; // leiratkoztatás, "gyűjtőből kivétel"

            szovegFeldolgozo("Másik Teszt Üzenet");


            MuveletKozvetito muveletVegzo = null;
            muveletVegzo += Negyzet;  // így is lehet írni (feliratkoztatás, "gyűjtőbe belerakás")
            muveletVegzo += Gyok;

            double pi = 3.14;
            ;
            double piNegyzet = muveletVegzo(pi);
            Console.WriteLine(piNegyzet);

            //szovegFeldolgozo += Negyzet; // hibás, nem egyezik a szignatúra



            /* Metódus átadása paraméterként */

            KulsoMuvelet muvelet = Megjelenit;
            AtadasTeszt(muvelet);

            AtadasTeszt(OsszegMegjelenit);

            Console.ReadLine();
        }

        static void KiirKicsi(string szoveg)
        {
            Console.WriteLine(szoveg.ToLower());
        }

        static void KiirNagy(string s)
        {
            Console.WriteLine(s.ToUpper());
        }



        static double Negyzet(double szam)
        {
            //return Math.Pow(szam, 2);
            return szam * szam;
        }

        static double Gyok(double szam)
        {
            return Math.Sqrt(szam);
        }



        static void AtadasTeszt(KulsoMuvelet muvelet)
        {
            int x = 5, y = 4;
            if (muvelet != null)
                muvelet(x, y); // a paraméterben átvett metódustól függően mindig más és más műveletet hajthatunk végre az itt lévő adatokon
        }

        static void Megjelenit(int egyik, int masik)
        {
            Console.WriteLine(egyik + ", " + masik);
        }

        static void OsszegMegjelenit(int egyik, int masik)
        {
            int osszeg = egyik + masik;
            Console.WriteLine(osszeg);
        }
    }
}