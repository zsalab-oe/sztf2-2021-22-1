﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_Esemenykezeles_Delegate_Event
{
    class Lottozo
    {
        public delegate void SorsolasKezelo(string uzenet);

        /* 1. verzió: publikus delegált */

        //public SorsolasKezelo nyert;



        /* 2. verzió: privát delegált + publikus le- és feliratkoztató metódusok */

        //SorsolasKezelo nyert;

        //public void Feliratkozas(SorsolasKezelo feliratkozo)
        //{
        //    nyert += feliratkozo; // feliratkoztatás, "gyűjtőbe belerakás"
        //}

        //public void Leiratkozas(SorsolasKezelo leiratkozo)
        //{
        //    nyert -= leiratkozo; // leiratkoztatás, "gyűjtőből kivétel"
        //}



        /* 3. verzió: event használata */

        public event SorsolasKezelo nyert;
        // esemény a delegált alapján
        // a háttérben az 'event' kiváltja a fel- és leiratkoztatást, valamint, hogy private-ként vesszük a delegáltat!

        public Lottozo()
        {
            nyert = null;
        }

        public virtual void Sorsolas()
        {
            Random rnd = new Random();
            if (rnd.Next(0,10) < 8)
            {
                //nyert?.Invoke("Nyertél!");
                OnNyert();
            }
        }

        protected void OnNyert()
        {
            //if (nyert != null) // a vizsgálat azért kell, hogy ha nincs semmi sem feliratkozva, ne álljon le futásidejű hibával
            //    nyert("Nyertél!"); // esemény "elsütése" (jelzés az eseményről)

            nyert?.Invoke("Nyertél!");
        }
    }

    class OnlineLottozo : Lottozo
    {
        public override void Sorsolas()
        {
            Random rnd = new Random();
            if (rnd.Next(0, 10) < 1)
            {
                OnNyert();
            }
        }
    }
}