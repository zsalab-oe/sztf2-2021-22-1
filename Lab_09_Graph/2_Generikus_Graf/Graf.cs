﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Generikus_Graf
{
    abstract class Graf<T>
    {
        public delegate void Muvelet(T tartalom);

        protected int N;

        protected Graf(int N)
        {
            this.N = N;
        }

        public abstract List<T> Csucsok();
        public abstract void ElFelvetel(T honnan, T hova);
        public abstract bool VezetEl(T honnan, T hova);

        public List<T> Szomszedok(T csucs)
        {
            List<T> l = new List<T>();
            foreach (T cs in Csucsok())
            {
                if (VezetEl(csucs, cs))
                {
                    l.Add(cs);
                }
            }
            return l;
        }

        public void SzelessegiBejaras(T start, Muvelet metodus)
        {
            Muvelet _metodus = metodus;

            Queue<T> S = new Queue<T>();
            List<T> F = new List<T>();
            S.Enqueue(start);
            F.Add(start);
            while (S.Count != 0)
            {
                T k = S.Dequeue();
                _metodus?.Invoke(k);
                foreach (T x in Szomszedok(k))
                {
                    if (!F.Contains(x))
                    {
                        S.Enqueue(x);
                        F.Add(x);
                    }
                }
            }
        }
    }
}
