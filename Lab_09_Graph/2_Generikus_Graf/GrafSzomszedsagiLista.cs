﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Generikus_Graf
{
    class GrafSzomszedsagiLista<T> : Graf<T>
    {
        List<T> csucsok;    // csúcsok listája
        List<int>[] L;      // szomszédsági listák tömbje

        public GrafSzomszedsagiLista(int N, List<T> csucsok) : base(N)
        {
            L = new List<int>[N];
            this.csucsok = csucsok;
            for (int i = 0; i < N; i++)
            {
                L[i] = new List<int>();
            }
        }

        public override List<T> Csucsok()
        {
            return csucsok;
        }

        public override void ElFelvetel(T honnan, T hova)
        {
            L[csucsok.IndexOf(honnan)].Add(csucsok.IndexOf(hova));
            L[csucsok.IndexOf(hova)].Add(csucsok.IndexOf(honnan));
        }

        public override bool VezetEl(T honnan, T hova)
        {
            return L[csucsok.IndexOf(honnan)].Contains(csucsok.IndexOf(hova));
        }
    }
}
