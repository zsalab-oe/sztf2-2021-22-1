﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Generikus_Graf
{
    class Auto
    {
        public string Rendszam { get; private set; }

        public Auto(string nev)
        {
            Rendszam = nev;
        }

        public override string ToString()
        {
            return Rendszam;
        }
    }
}
